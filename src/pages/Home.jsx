import React from 'react'
// import images from '../images/images'
import styleGlobal from '../pages/Global.css'
import homeStyle from './Home.css' 
import {search,notifications} from 'ionicons/icons'
import headerHome from '../images/headerHome.png'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonIcon } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
export default function Home() {
  return (
    <IonPage>
    <IonHeader>
      <IonToolbar>
        <IonTitle>Home</IonTitle>
      </IonToolbar>
    </IonHeader>
    <IonContent fullscreen>
      <div className="header">
        <img src={headerHome} alt="" />
        <div className="containerHeader">
          <div>
              <div className='fs-20'>
                Hi,Nandiya !
              </div>
              <div className="fs-12">
                30000 Point | Platinum
              </div>
          </div>
          <div className='fs-25'>
            <IonIcon icon={search}/>
            <IonIcon className="ml-10" icon={notifications}/>
          </div>
        </div>
      </div>
    </IonContent> 
  </IonPage>
  )
}

